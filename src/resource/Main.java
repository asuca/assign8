package resource;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class Main {
	public static void main(String[] args) {
		List<Address> address = new ArrayList<Address>();
		address.add(new Address("3203","provost","montreal","QC","H8T1R1"));
		address.add(new Address("2203","provost","montreal","QC","H8T3R3"));
		address.add(new Address("1203","provost","montreal","QC","H8T8R8"));
		address.add(new Address("4201","provost","montreal","QC","H8T4R4"));
		address.add(new Address("5205","provost","montreal","QC","H8T7R7"));
		//method
		Main obj = new Main();
		obj.writeToFile(address);	
	}
	public void writeToFile(List<Address> address) {
		try {
			FileOutputStream   fop = new FileOutputStream("resource/resource.txt");
			OutputStreamWriter outPutWriter = new OutputStreamWriter(fop,"UTF-16");
			BufferedWriter  bufferWriter = new BufferedWriter(outPutWriter);
			//address.forEach((n) -> bufferWriter.write(n.toString()));
//			for (int i = 0; i < address.size(); i++) {
//				bufferWriter.write(address.get(i).toString());
//				bufferWriter.newLine();
//				bufferWriter.flush();
//			}
			for (Address str:address) {
				bufferWriter.write(str.toString());
				bufferWriter.newLine();
				bufferWriter.flush();
			}
			bufferWriter.close();
		}catch (IOException e){
			e.printStackTrace();
		}
		
	}
}
