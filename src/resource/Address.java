package resource;

public class Address {
	private String buildingNo;
	private String streetName;
	private String cityName;
	private String provinceName;
	private String postalCode;
	
	public Address(String buildingNo,String streetName,String cityName,
			String provinceName,String postalCode) {
		this.buildingNo   = buildingNo;
		this.streetName   = streetName;
		this.cityName     = cityName;
		this.provinceName = provinceName;
		this.postalCode   = postalCode;
	}
	
	public String getBuildingNo() {
		return buildingNo;	
	}
	
	public String getStreetName() {
		return streetName;
	}
	
	public String getCityName() {
		return cityName;
	}
	
	public String getProvinceName() {
		return provinceName;
	}
	
	public String getPostalCode() {
		return  postalCode;
	}
	
	public String toString() {
		return "Address = " + buildingNo + " , " + 
					streetName + " , " + cityName + " , " + provinceName + " , " + postalCode;
	}
}
